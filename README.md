Word Frequency
==============
Word Frequency program reads the contents of given wikipedia website page and reports top words on the page.
A word is defined as a sequence of at least four alphabetic characters.

The parameters to the program is `page_id - of the website` and `n - top words`. 
* Example :  n = 5 and page_id = 21721040 

* Sample URL: [Stackoverflow](https://en.wikipedia.org/w/api.php?action=query&prop=extracts&pageids=21721
040&explaintext&format=json)

The output lists top n words and when two or more words have the same frequency, they are listed on the same line separated
by a comma. 

To test the mocks nocontent.json and stackoverflow.json files are used.

Project details
---------------

- Java 1.8
- Gradle build tool, 3.3
- Bitbucket repository
- Jacoco plugin
- Junit, Mockito 

Project Repository 
------------------
- Use the below command to clone the project from bitbucket to your local folder

		git clone https://bitbucket.org/preetham0411/wordfrequency.git
		
- Or Download the project using the link [WordFrequency](https://bitbucket.org/preetham0411/wordfrequency/downloads/)

Build Project
-------------
- Go to the project root directory and run the below command to clean build the project
	
		gradle clean build

Run Test cases
--------------
- Go to the project root directory and run the below command to execute test cases

		gradle test

- Test reports can be found under `${project.dir}/build/reports/tests/`
- WikipediaPageWordFrequencyApiTest class is used to for integration testing.  
- WikipediaPageWordFrequencyTest, InputParameterValidatorTest and RestTemplate Test is used for unit testing.

Generate Code Coverage
----------------------
- Go to the project root directory and run the below command to generate code coverage

		gradle jacoco

- Coverage reports can be found under `${project.dir}/build/reports/jacoco/` 
 
Static Code Analysis
--------------------
- Static code analysis is conducted on the fly using sonarlint plugin on eclipse.
