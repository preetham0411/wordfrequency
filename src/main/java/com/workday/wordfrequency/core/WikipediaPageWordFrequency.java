package com.workday.wordfrequency.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.workday.wordfrequency.exception.ContentNotAvailableException;
import com.workday.wordfrequency.rest.RestTemplate;
import com.workday.wordfrequency.validator.InputParameterValidator;

public class WikipediaPageWordFrequency {

	private static final Logger LOGGER = LoggerFactory.getLogger(WikipediaPageWordFrequency.class.getName());
	private static final String URL = "https://en.wikipedia.org/w/api.php?action=query&prop=extracts&explaintext&format=json";
	
	private RestTemplate rest;
	
	public WikipediaPageWordFrequency(RestTemplate rest) {
		this.rest = rest;
	}
	
	/**
	 * findNTopWords method is used to find the N top words on the wikipedia
	 * page. The page details on the wikipedia is found using the pageId
	 * parameter. The method returns list of N top words along with their
	 * frequency of occurrence.
	 * 
	 * @param n
	 * @param pageId
	 * @return
	 * @throws ContentNotAvailbaleException
	 */
	public List<Map.Entry<Integer, String>> findNTopWords(final int n, final int pageId) throws ContentNotAvailableException {
		InputParameterValidator.validate(n, pageId);
		final List<NameValuePair> nvps = new ArrayList<>();
		nvps.add(new BasicNameValuePair("pageids", String.valueOf(pageId)));
		final String content = rest.get(URL, nvps);
		final JsonPath path = JsonPath.compile("$.query.pages." + pageId + ".extract");
		String extract;
		try {
			extract = path.read(content);
		} catch(Exception e) {
			LOGGER.error(e.getMessage(), e.getCause());
			throw new ContentNotAvailableException(e.getMessage(), e.getCause());
		}
		final String title = JsonPath.parse(content).read("$.query.pages." + pageId + ".title");
		LOGGER.info(title);
		final Map<String, Integer> wordFrequency = new HashMap<>();
		List<String> words = Arrays.asList(extract.toLowerCase().split("\\s+|-|\\?|,|%|\\(|\\)|:|'|\"|\\."));
		words = words.stream().filter(word -> word.length() > 3)
				.collect(Collectors.toList());
		if(LOGGER.isInfoEnabled())
			LOGGER.info("Total number of words having with atleast 4 characters - " + words.size());
		words.stream().map(this::mapByKey).reduce(wordFrequency, this::reduceByKey);
		final Map<Integer, String> frequencyWords = wordFrequency.entrySet().stream().map(this::mapByValue)
				.reduce(this::reduceByValue).get();
		if(LOGGER.isInfoEnabled())
			LOGGER.info("frequency obtained - " + frequencyWords);
		final List<Map.Entry<Integer, String>> nTopWords = frequencyWords.entrySet().stream()
				.sorted(new WordFrequencyComparatorDescending()).limit(n).collect(Collectors.toList());
		System.out.println(String.format("URL: %s&pageids=%s", URL, pageId));
		System.out.println("Title: " + title);
		System.out.println(String.format("Top %d words:", n));
		nTopWords.stream().forEach(frequencyAndWords -> System.out
				.println(String.format("- %d %s", frequencyAndWords.getKey(), frequencyAndWords.getValue())));
		return nTopWords;
	}
	
	private Map<String, Integer> mapByKey(String word) {
		Map<String, Integer> wordCount = new HashMap<>();
		wordCount.put(word, 1);
		return wordCount;
	}

	private Map<String, Integer> reduceByKey(Map<String, Integer> x, Map<String, Integer> y) {
		for (Map.Entry<String, Integer> wordEntrySet : y.entrySet()) {
			if (x.containsKey(wordEntrySet.getKey())) {
				x.put(wordEntrySet.getKey(), x.get(wordEntrySet.getKey()) + 1);
			} else {
				x.put(wordEntrySet.getKey(), 1);
			}
		}
		return x;
	}

	private Map<Integer, String> mapByValue(Map.Entry<String, Integer> wordset) {
		Map<Integer, String> x = new HashMap<>();
		x.put(wordset.getValue(), wordset.getKey());
		return x;
	}

	private Map<Integer, String> reduceByValue(Map<Integer, String> x, Map<Integer, String> y) {
		for (Map.Entry<Integer, String> wordEntrySet : y.entrySet()) {
			if (x.containsKey(wordEntrySet.getKey())) {
				x.put(wordEntrySet.getKey(), x.get(wordEntrySet.getKey()) + ", " + wordEntrySet.getValue());
			} else {
				x.put(wordEntrySet.getKey(), wordEntrySet.getValue());
			}
		}
		return x;
	}

	private final class WordFrequencyComparatorDescending implements Comparator<Map.Entry<Integer, String>> {
		@Override
		public int compare(Map.Entry<Integer, String> o1, Map.Entry<Integer, String> o2) {
			return Integer.compare(o2.getKey(), o1.getKey());
		}
	}

}
