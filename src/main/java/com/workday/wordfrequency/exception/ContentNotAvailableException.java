package com.workday.wordfrequency.exception;

public class ContentNotAvailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6720032825021786833L;
	
	public ContentNotAvailableException(String message, Throwable t) {
		super(message, t);
	}

	public ContentNotAvailableException(String message) {
		super(message);
	}

}
