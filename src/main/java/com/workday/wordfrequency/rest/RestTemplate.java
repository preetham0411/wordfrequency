package com.workday.wordfrequency.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.workday.wordfrequency.exception.ContentNotAvailableException;

public class RestTemplate {
	private static final Logger LOGGER = LoggerFactory.getLogger(RestTemplate.class.getName());
	
	private final HttpClient httpClient;
	
	public RestTemplate(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	
	/**
	 * Returns httpClient set in the constructor
	 * @return
	 */
	public HttpClient getHttpClient() {
		return httpClient;
	}

	/**
	 * get method invokes http get call. Returns content read from the response entity.
	 * @param url
	 * @param nvps
	 * @return response
	 * @throws ContentNotAvailableException
	 */
	public String get(final String url, final List<NameValuePair> nvps) throws ContentNotAvailableException {
		String result;
		try {
			final URI uri =  new URIBuilder(url).addParameters(nvps).build();
			final HttpGet request = new HttpGet(uri);
			final HttpResponse response = httpClient.execute(request);
			final int responseCode = response.getStatusLine().getStatusCode();
			if(LOGGER.isInfoEnabled())
				LOGGER.info(String.format("Response Code - %s ", responseCode));
			if (!(responseCode >= 200 && responseCode < 300)) {
				throw new ContentNotAvailableException("Response not valid "+responseCode);
			}
			result = readResponse(response);
			if(LOGGER.isInfoEnabled())
				LOGGER.info("Reult obtained - "+result);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e.getCause());
			throw new ContentNotAvailableException(e.getMessage(), e.getCause());
		}
		return result;
	}

	private String readResponse(final HttpResponse response) throws IOException {
		final BufferedReader rd = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
		final StringBuilder result = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		return result.toString();
	}
	
}
