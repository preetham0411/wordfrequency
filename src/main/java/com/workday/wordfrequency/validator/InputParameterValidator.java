package com.workday.wordfrequency.validator;

public final class InputParameterValidator {
	
	private InputParameterValidator() {
	}
	
	private InputParameterValidator checkIfNValid(int n) {
		if(n <= 0 ) {
			throw new IllegalArgumentException("Number of top words to find cannot be <= 0");
		}
		return this;
	}
	
	private InputParameterValidator checkIfPageIdValid(final int pageId) {
		if (pageId <= 0) {
			throw new IllegalArgumentException("Page id cannot be <= 0");
		}
		return this;
	}
	
	public static final void validate(final int n, final int pageId) {
		new InputParameterValidator().checkIfNValid(n).checkIfPageIdValid(pageId);
	}
}
