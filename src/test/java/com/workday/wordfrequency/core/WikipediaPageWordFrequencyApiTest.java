package com.workday.wordfrequency.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.workday.wordfrequency.exception.ContentNotAvailableException;
import com.workday.wordfrequency.rest.RestTemplate;

public class WikipediaPageWordFrequencyApiTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(WikipediaPageWordFrequencyApiTest.class.getName());
	
	private List<Map.Entry<Integer, String>> wordFrequency;
	private RestTemplate rest;
	
	@Before
	public void setUp() {
		rest = new RestTemplate(HttpClientBuilder.create().build());
	}
	
	
	@Test
	public void testFind5TopWords() {
		final WikipediaPageWordFrequency wordFrequencyFinder =  new WikipediaPageWordFrequency(rest);
		try {
			wordFrequency = wordFrequencyFinder.findNTopWords(5, 21721040);
			assertEquals(5, wordFrequency.size());
		} catch (ContentNotAvailableException e) {
			LOGGER.error(e.getMessage());
			fail("Unexpted Error");
		}
	}
	
	@Test
	public void testFind7TopWords() {
		final WikipediaPageWordFrequency wordFrequencyFinder =  new WikipediaPageWordFrequency(rest);
		try {
			wordFrequency = wordFrequencyFinder.findNTopWords(7, 21721040);
			assertEquals(7, wordFrequency.size());
		} catch (ContentNotAvailableException e) {
			LOGGER.error(e.getMessage());
			fail("Unexpected Error");
		}
	}
	
	
	@Test
	public void testInvalidNParameter() {
		final WikipediaPageWordFrequency wordFrequencyFinder =  new WikipediaPageWordFrequency(rest);
		try {
			wordFrequency = wordFrequencyFinder.findNTopWords(0, 21721040);		
			assertEquals(true, wordFrequency.isEmpty());
		} catch (IllegalArgumentException | ContentNotAvailableException e) {
			LOGGER.error(e.getMessage());
			assert(e.getMessage().equals("Number of top words to find cannot be <= 0"));
		}
	}
	
	@Test
	public void testInvalidPageIdParameter1() {
		final WikipediaPageWordFrequency wordFrequencyFinder =  new WikipediaPageWordFrequency(rest);
		try {
			wordFrequency = wordFrequencyFinder.findNTopWords(5, 0);		
		} catch (IllegalArgumentException | ContentNotAvailableException e) {
			LOGGER.error(e.getMessage());
			assert(e.getMessage().equals("Page id cannot be <= 0"));
		}
	}
	
	@Test
	public void testInvalidPageIdParameter2() {
		final WikipediaPageWordFrequency wordFrequencyFinder =  new WikipediaPageWordFrequency(rest);
		final int pageId = 222222222;
		try {
			wordFrequency = wordFrequencyFinder.findNTopWords(5, pageId);		
		} catch (ContentNotAvailableException e) {
			LOGGER.error(e.getMessage());
			assert(e.getMessage().equals(String.format("No results for path: $['query']['pages']['%d']['extract']", pageId)));
		}
	}
}
