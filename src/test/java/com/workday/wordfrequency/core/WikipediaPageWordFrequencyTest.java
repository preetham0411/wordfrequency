package com.workday.wordfrequency.core;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.jsonpath.JsonPath;
import com.workday.wordfrequency.exception.ContentNotAvailableException;
import com.workday.wordfrequency.rest.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class WikipediaPageWordFrequencyTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(WikipediaPageWordFrequencyTest.class.getName());
	
	private static final String URL = "https://en.wikipedia.org/w/api.php?action=query&prop=extracts&explaintext&format=json";
	private int pageId = 21721040;
	private List<NameValuePair> nvps;
	private RestTemplate rest;
	private WikipediaPageWordFrequency wiki;
	
	@Before
	public void setup() {
		nvps = new ArrayList<>();
		nvps.add(new BasicNameValuePair("pageids", String.valueOf(pageId)));
		rest = Mockito.mock(RestTemplate.class);
		wiki = new WikipediaPageWordFrequency(rest);
	}
	
	@Test
	public void testFindNTopWords() {
		try {
			final String json = JsonPath.parse(new File("stackoverflow.json")).jsonString();
			LOGGER.info(json);
			Mockito.when(rest.get(URL, nvps)).thenReturn(json);
			List<Map.Entry<Integer, String>> words = wiki.findNTopWords(10, pageId);
			assert(words.size() > 0);
		} catch (ContentNotAvailableException | IOException e) {
			LOGGER.error(e.getMessage());
			fail("Unexpected Error");
		}
	}
	
	@Test
	public void testFindNTopWordsFail() {
		final int pageId = 222222222;
		try {
			nvps.set(0, new BasicNameValuePair("pageids", String.valueOf(pageId)));
			final String json = JsonPath.parse(new File("nocontent.json")).jsonString();
			LOGGER.info(json);
			Mockito.when(rest.get(URL, nvps)).thenReturn(json);
			wiki.findNTopWords(10, pageId);
		} catch (ContentNotAvailableException | IOException e) {
			LOGGER.error(e.getMessage());
			assert(e.getMessage().equals(String.format("No results for path: $['query']['pages']['%d']['extract']", pageId)));
		}
	}

}
