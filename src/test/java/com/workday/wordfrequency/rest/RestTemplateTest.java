package com.workday.wordfrequency.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.workday.wordfrequency.exception.ContentNotAvailableException;

public class RestTemplateTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestTemplateTest.class.getName());
	private static final String URL = "https://en.wikipedia.org/w/api.php?action=query&prop=extracts&explaintext&format=json";
	private int pageId = 21721040;
	private List<NameValuePair> nvps;
	private CloseableHttpClient httpClient;
	private RestTemplate restTemplate;
	private CloseableHttpResponse response;
	private HttpEntity entity;
	private StatusLine statusline;

	@Before
	public void setup() {
		httpClient = Mockito.mock(CloseableHttpClient.class);
		nvps = new ArrayList<>();
		nvps.add(new BasicNameValuePair("pageids", String.valueOf(pageId)));
		restTemplate = new RestTemplate(httpClient);
		response = Mockito.mock(CloseableHttpResponse.class);
		entity = Mockito.mock(HttpEntity.class);
		statusline = Mockito.mock(StatusLine.class);
	}

	@Test
	public void tesetGet1() {
		try {
			Mockito.when(httpClient.execute(Mockito.any())).thenReturn(response);
			Mockito.when(response.getStatusLine()).thenReturn(statusline);
			Mockito.when(statusline.getStatusCode()).thenReturn(HttpStatus.SC_OK);
			Mockito.when(response.getEntity()).thenReturn(entity);
			Mockito.when(entity.getContent()).thenReturn(new FileInputStream(new File("stackoverflow.json")));
			final String content = restTemplate.get(URL, nvps);
			assert (content.contains("21721040"));
		} catch (IOException | ContentNotAvailableException e) {
			LOGGER.error(e.getMessage(), e);
			fail("Unexpected Error");
		}
	}

	@Test
	public void testGet2() {
		nvps.set(0, new BasicNameValuePair("pageids", String.valueOf(222222222)));
		try {
			Mockito.when(httpClient.execute(Mockito.any(HttpUriRequest.class))).thenReturn(response);
			Mockito.when(response.getEntity()).thenReturn(entity);
			Mockito.when(response.getStatusLine()).thenReturn(statusline);
			Mockito.when(statusline.getStatusCode()).thenReturn(HttpStatus.SC_OK);
			Mockito.when(entity.getContent()).thenReturn(new FileInputStream(new File("nocontent.json")));
			final String content = restTemplate.get(URL, nvps);
			assert (content.contains("missing"));
		} catch (IOException | ContentNotAvailableException e) {
			LOGGER.error(e.getMessage());
			fail("Unexpected Error");
		}
	}

	@Test
	public void testGet3() {
		try {
			Mockito.when(httpClient.execute(Mockito.any(HttpUriRequest.class))).thenReturn(response);
			Mockito.when(response.getEntity()).thenReturn(entity);
			Mockito.when(response.getStatusLine()).thenReturn(statusline);
			Mockito.when(statusline.getStatusCode()).thenReturn(HttpStatus.SC_BAD_REQUEST);
			restTemplate.get(URL, nvps);
			fail("Expecting an exception to be caught in catch block");
		} catch (IOException | ContentNotAvailableException e) {
			LOGGER.error(e.getMessage());
			assertEquals(e.getMessage(), String.format("Response not valid %d", HttpStatus.SC_BAD_REQUEST));
		}
	}

	@Test
	public void testGetWhenNoConnection() {
		try {
			Mockito.when(httpClient.execute(Mockito.any(HttpUriRequest.class)))
					.thenThrow(new UnknownHostException("No internet connection"));
			restTemplate.get(URL, nvps);
			fail("Expecting an exception to be caught in catch block");
		} catch (IOException | ContentNotAvailableException e) {
			LOGGER.error(e.toString());
			assert (e instanceof ContentNotAvailableException);
		}
	}

}
