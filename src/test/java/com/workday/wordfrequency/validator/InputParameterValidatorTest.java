package com.workday.wordfrequency.validator;

import static org.junit.Assert.*;

import org.junit.Test;

public class InputParameterValidatorTest {
	@Test
	public void testNInputFailTest() {
		try {
			InputParameterValidator.validate(0, 222222);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Number of top words to find cannot be <= 0");
		}
		
	}
	
	@Test
	public void testPageidInputFail() {
		try {
			InputParameterValidator.validate(5, 0);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Page id cannot be <= 0");
		}
		
	}
}
